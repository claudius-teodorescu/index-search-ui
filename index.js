import { LitElement, html, css } from "https://cdn.jsdelivr.net/npm/lit/+esm";
import { Task } from "https://cdn.jsdelivr.net/npm/@lit/task@1.0.0/+esm";
import * as SparqlQueries from "./queries.js";
import init_searcher, { PhilologicalIndexSearcher } from "https://claudius-teodorescu.gitlab.io/index-search-engine/index_search_engine.js";
await init_searcher();

import jsUntar from "https://cdn.jsdelivr.net/npm/js-untar@2.0.0/+esm";
import "../autocompleter/index.js";
import init_oxigraph, * as oxigraph from "https://cdn.jsdelivr.net/npm/oxigraph@0.4.0-alpha.4/+esm";
await init_oxigraph();

const styles =
  css`
        :host {
            width: var(--kub-width, 40vw);
            display: flex;
            flex-direction: column; 
            gap: 5px;
            align-items: center; 
        }

        div#autocompleter-container {
            display: flex;
            justify-content: space-between;
            gap: 5vw;
            width: 100%;
        }

        sl-select#index-select {
            width: calc(var(--kub-width) / 3);
        }

        wc-autocompleter {
            width: calc(var(--kub-width) / 2);
        }
        #search_facets_toolbar {
            display: none;
            flex-wrap: wrap;
        }
        button.search-facet {
            min-width: 30px;
            display: inline-block;
            text-align: center;
            background-color: var(--kub-first-facet-btn-bg-color, #8a8a8a);
            color: white;
        }         
        /* small screen sizes */
        @media only screen and (max-width: 664px) {
            wc-autocompleter {
                --kub-width: 90vw;
            }
        }
        /* medium screen sizes */
        @media only screen and (max-width: 835px) {
            wc-autocompleter {
                --kub-width: 90vw;
            }  
        }        
`;

export default class RdfGraphSearcher extends LitElement {
  static properties = {
    /** The base URL for the index. */
    graphBaseURL: {
      attribute: "graph-base-url"
    },
    graphBaseURL2: {
      attribute: "graph-base-url2"
    },
    _current_index: {
      type: Object,
      // only update for odd values of newVal.
      hasChanged(newVal, oldVal) {
        const hasChanged = newVal % 2 == 1;
        console.log(`${newVal}, ${oldVal}, ${hasChanged}`);
        console.log(this);
        return hasChanged;
      },
    },
  };
  //this.requestUpdate();

  static styles = styles;

  constructor() {
    super();

    // aggregated metadata
    this._aggregated_metadata = {};

    // current index
    this._current_index_metadata = {};

    // the Oxigraph store
    this._metadata_store = new oxigraph.Store();

    // form controls
    this._index_selector_form_control = null;
    this._autocomplete_form_control = null;
    this._search_facets_toolbar = null;

    // the searchers
    this._searcher = null;
  }

  async connectedCallback() {
    super.connectedCallback();
  }

  render() {
    return html`
        <div id="autocompleter-container">
          ${this._load_aggregated_metadata.render({
      pending: () => html`Loading indexes...`,
      complete: (data) => html`<sl-select id="index-select" value="${data.value}" help-text="Selectare index" placeholder="Selectare index">${data.options}</sl-select`,
    })}
          <wc-autocompleter></wc-autocompleter>
        </div>
        <div id="search_facets_toolbar"></div>
    `;
  }

  createRenderRoot() {
    const root = super.createRenderRoot();

    root.addEventListener("wc-autocompleter:query-string", event => {
      let queryString = event.detail;

      let processed_query_string = this._process_query_string(queryString);
      
      console.time("autocompletion");
      let headings = this._searcher.headings_by_heading_regex(processed_query_string);
      headings = [...headings.entries()].sort((a, b) => new Intl.Collator("ro").compare(a[0], b[0]));
      console.timeEnd("autocompletion");

      if (headings.length > 10) {
        document.dispatchEvent(new CustomEvent("kub-rdf-graph-searcher:prefix-search-result", {
          "detail": {
            queryString,
            headings
          }
        }));
      } else {
        this._autocomplete_form_control.datalist = headings;
      }
    });

    root.addEventListener("wc-autocompleter:selected", event => {
      let detail = event.detail;

      document.dispatchEvent(new CustomEvent("kub-rdf-graph-searcher:exact-search-result", {
        "detail": [detail.suggestion, detail.id]
      }));
    });

    root.addEventListener("wc-autocompleter:state-changed", event => {
      this.dispatchEvent(new CustomEvent("kub-rdf-graph-searcher:state-changed", {
        "bubbles": true,
        "composed": true,
      }));
    });

    root.addEventListener("click", event => {
      const target = event.target;

      if (target.matches("button.search-facet")) {
        this._autocomplete_form_control.reset();
        this.dispatchEvent(new CustomEvent("kub-rdf-graph-searcher:state-changed", {
          "bubbles": true,
          "composed": true,
        }));

        let queryString = target.dataset.value;
        let processed_query_string = `${queryString}.*`;

        console.time("search-facet");
        let headings = this._searcher.headings_by_heading_regex(processed_query_string);
        headings = [...headings.entries()].sort((a, b) => new Intl.Collator("ro").compare(a[0], b[0]));
        console.timeEnd("search-facet");

        document.dispatchEvent(new CustomEvent("kub-rdf-graph-searcher:prefix-search-result", {
          "detail": {
            queryString,
            headings
          }
        }));
      }
    });

    root.addEventListener("sl-change", async event => {
      const target = event.target;

      if (target.matches("sl-select#index-select")) {
        this.dispatchEvent(new CustomEvent("kub-rdf-graph-searcher:state-changed", {
          "bubbles": true,
          "composed": true,
        }));
        this._autocomplete_form_control.reset();

        let selected_value = target.value;
        let selected_option_element = target.querySelector(`sl-option[value = ${selected_value}]`);

        // load the selected index as curent index
        await this._load_selected_index(selected_option_element.dataset.indexMetadataUrl, selected_option_element.dataset.indexUrl);
      }


    });

    return root;
  }

  async firstUpdated() {
    // initialise the autocompleter
    this._autocomplete_form_control = this.renderRoot.querySelector("wc-autocompleter");

    this._index_headings_viewer = this.renderRoot.querySelector("kub-index-headings-viewer");
  }

  _load_aggregated_metadata = new Task(
    this,
    async ([]) => {
      let aggregated_metadata_graph = "";
      await fetch(new URL("metadata.ttl", this.graphBaseURL2), {
        mode: "cors",
      })
        .then(response => response.text())
        .then(async data => {
          aggregated_metadata_graph = data;
        });

      // load the aggregated metadata graph in Oxigraph store
      this._metadata_store.load(aggregated_metadata_graph, "text/turtle", null, oxigraph.Default);

      //extract some aggregated metadata
      let aggregated_metadata_bindings = this._metadata_store.query(SparqlQueries.aggregated_indexes_metadata);
      this._aggregated_metadata.indexes = [];

      for (const binding of aggregated_metadata_bindings) {
        for (let [metadata_name, metadata_value] of binding.entries()) {
          if (metadata_name === "default_index_metadata_url") {
            this._aggregated_metadata[metadata_name] = metadata_value.value;
          } else {
            this._aggregated_metadata.indexes.push(metadata_value.value);
          }
        }
      }

      let options = [];
      let select_value = "index-0";

      for (const [i, index_description] of this._aggregated_metadata.indexes.entries()) {
        let [index_url, index_title, index_metadata_url] = index_description.split("=");

        // set the default index URL
        if (this._aggregated_metadata.default_index_metadata_url === index_metadata_url) {
          select_value = `index-${i}`;

          // load the default index as curent index
          await this._load_selected_index(index_metadata_url, index_url);
        }
        options.push(html`<sl-option value="index-${i}"
          data-index-url="${index_url}"
          data-index-metadata-url="${index_metadata_url}">${index_title}</sl-option>`);
      }

      return {
        options,
        "value": select_value,
      };
    },
    () => []
  );

  _load_selected_index = async (index_metadata_url, index_url) => {
    // load the index metadata
    // extract the search facets, with labels and regular expressions
    let search_facets = [];
    for (const binding of this._metadata_store.query(SparqlQueries.search_facets(index_metadata_url))) {
      search_facets.push(binding.get("search_facet").value);
    }
    search_facets.sort(new Intl.Collator("ro").compare);

    // if they are search facets, initialise the search facets toolbar
    if (search_facets.length > 0) {
      this._search_facets_toolbar = this.renderRoot.getElementById("search_facets_toolbar");

      // fill in the search facets toolbar
      let search_facets_toolbar_string = search_facets
        .map(search_facet => this._search_facet_button_template(search_facet.split("=")))
        .join("");
      this._search_facets_toolbar.innerHTML = "";
      this._search_facets_toolbar.insertAdjacentHTML("afterbegin", search_facets_toolbar_string);
      this._search_facets_toolbar.style.display = "flex";

      this._current_index_metadata.search_facets = search_facets;
    }

    // extract the character normalization mappings
    let character_normalization_mappings = [];
    for (const binding of this._metadata_store.query(SparqlQueries.character_normalization_mappings)) {
      character_normalization_mappings.push(binding.get("character_normalisation_mapping").value);
    }

    // process the character normalization mappings, to obtain the search groups
    let search_groups = {};
    for (const character_normalization_mapping of character_normalization_mappings) {
      let [character, normalized_character] = character_normalization_mapping.split("=");

      if (search_groups.hasOwnProperty(normalized_character)) {
        search_groups[normalized_character] = `${search_groups[normalized_character]}|${character}`;
      } else {
        search_groups[normalized_character] = `${normalized_character}|${character}`;
      }
    }
    for (let [key, value] of Object.entries(search_groups)) {
      search_groups[key] = `[${value}]`;
    }
    this._current_index_metadata.search_groups = search_groups;

    // extract the indexes and generate the searchers
    let index_archive_url = new URL(index_url.replace(".ttl", ".tar.gz"));
    await fetch(index_archive_url, {
      mode: "cors",
    })
      .then(response => response.blob())
      .then(data => {
        const decompressionStream = data.stream().pipeThrough(new DecompressionStream('gzip'));
        return new Response(decompressionStream).arrayBuffer();
      })
      .then(jsUntar)
      .then(async files => {
        let filtered_files = files.filter(item => item.name !== "./");
        let subjects_data = null;
        let objects_data = null;
        let triples_data = null;

        for (const file of filtered_files) {
          let file_name = file.name;
          let file_contents = await file.blob.arrayBuffer();
          file_contents = new Uint8Array(file_contents);

          switch (file_name) {
            case "subjects_section.fst":
              subjects_data = file_contents;
              break;
            case "objects_section.fst":
              objects_data = file_contents;
              break;
            case "id_triples.fst":
              triples_data = file_contents;
              break;
          }
        };

        this._searcher = new PhilologicalIndexSearcher(subjects_data, objects_data, triples_data);
      });
  }

  _change_index = () => {
    console.log("_change_index");
  }

  _process_query_string = (query_string) => {
    let query_string_tokens = query_string
      .split("")
      .filter(Boolean);
    query_string_tokens = query_string_tokens
      .map(token => {
        let search_groups = this._current_index_metadata.search_groups;
        if (search_groups.hasOwnProperty(token)) {
          return search_groups[token];
        } else {
          return token;
        }
      });

    return `${query_string_tokens.join("")}.*`;
  }

  _search_facet_button_template = ([label, regex]) =>
    `<button class="search-facet" title="${label}" data-value="${regex}">${label}</button>`;

  get_locators = (heading_id) => {
    let locators = this._searcher.locators_by_heading(heading_id);

    return locators;
  }
}

window.customElements.define("kub-rdf-graph-searcher", RdfGraphSearcher);
