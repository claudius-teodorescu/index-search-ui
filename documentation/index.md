# Documentation

## Table of Contents
1. [Description](#description)
2. [Overall Schema](#overall-schema)
3. [Components](#components)

## <div id="description" />1. Description

This user interface consists of web components with different functions, along with a search engined written in [Rust](https://rust-lang.org/) and compiled to [WebAssembly](https://webassembly.org/).

## <div id="overall-schema" />2. Overall Schema

The diagram below shows the main components of this user interface, along with the main workflow starting from the user input (```search string```), until a resource containing the results wanted by the user.

![Overall Schema](./overall-schema/overall-schema.drawio.png)

## <div id="components" />3. Components

All the web components are based upon the [LitElement](https://lit.dev/) library.

The indexes are created manually or automatically in the [RDF](https://w3.org/TR/rdf11-concepts/) format. The compressing of the indexes is done by using the module [index-as-triples-to-compressed-index](https://gitlab.com/claudius-teodorescu/index-as-triples-to-compressed-index). The syntax for each index is defined according to the [Indexing Ontology](https://claudius-teodorescu.gitlab.io/indexing-ontology/index-en.html) and observes the terminology specific to the field of indexing (```heading``` and ```locator```), namely it consist of triples as below:

```turtle
<locator> i:by "heading" .
<heading> i:at <locator> .
```

### The ```searcher``` components

#### Description

The ```searchers``` are web components dealing each of them with searching in one index. Each ```searcher``` generates a _triple pattern_ and, in this way, the ```kub-index-searching-ui``` component, as a whole, generates a _basic graph pattern_ (for details for _triple patterns_ and _basic graph pattern_, see the section [2.5 Basic Graph Patterns](https://www.w3.org/2001/sw/DataAccess/rq23/#BasicGraphPatternMatching) of the _SPARQL Query Language for RDF_ specification).

Each ```searcher``` has a ```@graph``` attribute, which has as value the IRI of the graph containing the index the ```searcher``` is associated with.

#### Examples of ```searcher``` components

* ```kub-exact-searcher```, which executes an exact search against the index. This searcher can have two appearances, as text input control or as select control, depending on the value of the attribute ```@apperance```, which can have the values of _input_ or _select_.

```sparql
prefix i: <https://kuberam.ro/ontologies/indexing#>

select ?heading ?locator #or only ?locator or only ?heading
from named <https://kuberam.ro/ontologies/indexing#graph-1/>
where
{
    graph i:graph-1 {
        ?locator i:by ?heading
    }
}
```

* ```kub-regex-searcher```, which executes a fuzzy search against the index, by using a regular expression. The search results are presented as list of suggestions (as in an autocompleter).

* ```kub-levenstein-searcher```, which executes a fuzzy search against the index, by using a Levenstein distance (the distance is mentioned in the attribute ```@n```, with possible values of _1_ or _2_, as a bigger distance would be too computationally expensive). For details about the Levenstein distance, see [Understanding the Levenshtein Distance Equation for Beginners](https://medium.com/@ethannam/understanding-the-levenshtein-distance-equation-for-beginners-c4285a5604f0). The search results are presented as list of suggestions (as in an autocompleter).

* ```kub-ngram-searcher```, which executes a fuzzy search against the index, by using ngrams (the ngram length is mentioned in the attribute ```@n```, and the threshold for the ngrams similarity between the string to search and the strings from the index is mentioned in the attribute ```@similarity-threshold```). For details about the ```similarity threshold```, also called the ```Dice coefficient``` see the article [N -Gram Similarity and Distance](https://webdocs.cs.ualberta.ca/~kondrak/papers/spire05.pdf), by Grzegorz Kondrak. The search results are presented as list of suggestions (as in an autocompleter).

* ```kub-substring-searcher```, which executes a fuzzy search against the index, by using substrings (prefixes, suffixes, or infixes). The type of the substring is mentioned in the attribute ```@type```, with possibile values of _prefix_, _suffix_, or _infix_. The search results are presented as list of suggestions (as in an autocompleter).

* ```sparql-searcher```, which executes a _basic graph pattern_ against all the indexes that are mentioned within it 

#### Query result format

The result of the searching is in a custom JSON format, and is unsorted. One can sort it in Javascript, further process it, and display it afterwards.

#### Example of a _basic graph pattern_

| single-index-searcher ||
| :---: | :---: |
|  index-selector | autocompleter |
|| search-type-selector |
|| facet-selector |

The components index-headings-viewr and text-segments-viewer are presented in two layouts: tabs, and details.

### multiple-index-searcher

| index-selector (select control) | headings (input control) | number of triples (output controls) | view results (button control) | add new line like this one (button control) | operator between lines?? |

Notes:

* The index-selector has to have as selected the default index;

* For headings, one can write regex(), ngram() etc.

* Use ```shacl-form``` for the searcher section (with button for displaying a mini editor to configure this section, and templates for this section, for user to choose from - this means that every user can have his own searcher) (the searcher section contains also the headings viewer, which can be also customized);

* Move searching in a web worker?;

* The ```locator-viewer``` to be configurable with a shacl-form? (This is in order to display either the IRI, or some information (author, date, etc) from the indexes for that IRI or an abstract of the resource with that IRI, brought with ```sl-load``` and containing the IRI to the actual resource, along with other metadata.).
