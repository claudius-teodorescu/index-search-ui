# Index Searching User Interface

## Scope

This component is designed to be a framework for designing user interfaces for searching in indexes loaded in browsers, by using a search engine also loaded in browser, and also for displaying the search results.

For more details about it, please see the [Documentation](./documentation/index.md).

## Change log
