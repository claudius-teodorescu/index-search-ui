const aggregated_indexes_metadata =
    `
    prefix i: <https://kuberam.ro/ontologies/text-index#>

    select ?default_index_metadata_url ?index_description
    where {
        ?default_index_metadata_url i:isDefaultIndex "true" .
        ?index_metadata_url i:metadataFor ?index_url .
        ?index_metadata_url i:title ?index_title .
        bind(concat(str(?index_url), '=', ?index_title, '=', str(?index_metadata_url)) as ?index_description)
    }
`;

const search_facets = (index_metadata_url) =>
    `
    prefix i: <https://kuberam.ro/ontologies/text-index#>
    prefix schema: <https://schema.org/>
    prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    select ?search_facet
    where {
        <${index_metadata_url}> i:searchFacet ?o .
        ?o schema:valuePattern ?regex .
        ?o rdfs:label ?label .
        bind(concat(?label, '=', ?regex) as ?search_facet)
    }
`;

const character_normalization_mappings = 
    `
    prefix i: <https://kuberam.ro/ontologies/text-index#>

    select distinct ?character_normalisation_mapping
    where {
        ?s i:characterNormalisationMapping ?character_normalisation_mapping
    }
`;

const search = (data) =>
    `
    base <${data.baseURI}>
    prefix i: <https://kuberam.ro/ontologies/text-edition-index/>

    select ?subject ?object where {
        ?subject <i:headword> ?object .
        filter (regex(lcase(str(?object)), "^${data.prefix_regex}.", "i"))
    }
`;

export {
    aggregated_indexes_metadata,
    search_facets,
    character_normalization_mappings,
    search,
};